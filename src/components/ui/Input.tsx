import { HTMLInputTypeAttribute, HtmlHTMLAttributes } from "react";
import { UseFormRegister, FieldErrors } from "react-hook-form";
type InputProps = {
  register?: UseFormRegister<any>;
  error?: string;
  name?: string;
  placeholder?: string;
  type: HTMLInputTypeAttribute;
};
export const Input = ({
  register,
  error,
  name,
  placeholder,
  type,
}: InputProps) => {
  return (
    <div className="mt-3">
      <input
        placeholder={placeholder}
        type={type}
        className="outline-nopne block w-full p-15 border text-white border-white rounded-lg bg-[#333] focus:ring-blue-600"
        {...register(name)}
      />
      {error && <p className="text-red-500"> {error}</p>}
    </div>
  );
};
