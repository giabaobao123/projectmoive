import styled from "styled-components";
import "../../assets/style.css";
import { NavLink, Navigate, useNavigate } from "react-router-dom";
import { Avatar, Popover } from ".";
import { UserOutlined } from "@ant-design/icons";
import { PATH } from "constant";
type Props = {};

export const Header = (props: Props) => {
  const navigate = useNavigate();
  return (
    <HeaderS>
      <div className="header-content flex items-center h-full justify-between">
        <h2 className="font-600 text-[30px]">CYBER MOIVE</h2>
        <div className="flex justify-around items-center basis-1/5 font-500">
          <NavLink className="link" to={"/About"}>
            About
          </NavLink>
          <NavLink className="link" to={"/Contact"}>
            Contact
          </NavLink>
          <Popover
            content={
              <div className="p-10">
                <h2 className="p-10 font-600 text-22">Đoàn Gia Bảo</h2>
                <hr />
                <div
                  onClick={() => navigate(PATH.account)}
                  style={{ color: "black" }}
                  className="p-10 cursor-pointer hover:bg-gray-500 text-16 hover:text-white transition-all duration-300"
                >
                  Thông tin cá nhân{" "}
                </div>
                <div className="p-10 cursor-pointer hover:bg-gray-500 text-16 hover:text-white transition-all duration-300">
                  Đăng xuất
                </div>
              </div>
            }
            trigger={"click"}
          >
            <Avatar
              className="cursor-pointer !flex !justify-center !items-center"
              size={40}
              icon={<UserOutlined />}
            />
          </Popover>
        </div>
      </div>
    </HeaderS>
  );
};
const HeaderS = styled.header`
  margin-bottom: 50px;
  height: var(--header-heigth);
  background: white;
  box-shadow: 0 0 5px rgba(1, 1, 1, 0.4);
  .header-content {
    max-width: var(--max-width);
    margin: auto;
    .link {
      transition: all 0.5s;
      &:hover {
        color: red;
      }
    }
  }
`;
