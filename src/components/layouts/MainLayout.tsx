import { Header } from "components/ui";
import React from "react";
import "../../assets/style.css";
import { Outlet } from "react-router-dom";
import styled from "styled-components";

const MainLayout = () => {
  return (
    <div>
      <Header />
      <Container>
        <Outlet />
      </Container>
    </div>
  );
};

export default MainLayout;
const Container = styled.div`
  max-width: var(--max-width);
  margin: auto;
`;
