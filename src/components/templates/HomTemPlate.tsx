import { Spin } from "antd";
import { Card, Skeleton } from "components/ui/";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { RootState, useAppDispatch } from "store";
import { getListFilmThunk } from "store/QuanLyFilm/Thunk";

export const HomTemPlate = () => {
  const dispatch = useAppDispatch();
  const { listFilm, isFetchMovie } = useSelector(
    (state: RootState) => state.quanLyPhimToolKit
  );
  useEffect(() => {
    dispatch(getListFilmThunk());
  }, [dispatch]);
  if (isFetchMovie) {
    // return (

    // <div className="!w-[350px]">
    //   <Skeleton active Image={'shape'} loading={true} className="!w-full !h-[350px]" />;
    // </div>
    // )
    return (
      <div
        className="loading flex items-center justify-center"
        style={{ background: "rgba(0,0,0,0.1)", height: "100vh" }}
      >
        <Spin className="example" />
      </div>
    );
  }
  return (
    <div className="">
      <div className="grid grid-cols-4 gap-x-100 gap-y-30">
        {listFilm.map((film) => {
          return (
            <Card
              hoverable
              style={{ width: 240 }}
              cover={<img alt="example" height={300} src={film.hinhAnh} />}
            >
              {" "}
              <Card.Meta
                title="Europe Street beat"
                description="www.instagram.com"
              />
            </Card>
          );
        })}
      </div>
    </div>
  );
};
