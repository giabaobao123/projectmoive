import { useRoutes } from "react-router-dom";
import { router } from "./router";
import { ToastContainer } from "react-toastify";
import { TaskAbortError } from "@reduxjs/toolkit";
function App() {
  return (
    <div>
      <ToastContainer />
      {useRoutes(router)}
    </div>
  );
}

export default App;
