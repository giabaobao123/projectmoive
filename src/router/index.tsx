import { RouteObject } from "react-router-dom";
import Demo from "../demo/Demo";
import { Logins, Register } from "Pages";
import { PATH } from "constant";
import { AuthLayout } from "components/layouts/index";
import Home from "Pages/Home";
import MainLayout from "components/layouts/MainLayout";
import { Children } from "react";
export const router: RouteObject[] = [
  {
    element: <MainLayout />,
    children: [
      {
        path: "/",
        element: <Home />,
      },
      {
        element: <AuthLayout />,
        children: [
          {
            path: PATH.login,
            element: <Logins />,
          },

          {
            path: PATH.resgister,
            element: <Register />,
          },
        ],
      },
    ],
  },
];
