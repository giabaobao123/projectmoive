import { createSlice } from '@reduxjs/toolkit'
import { LoginThunk } from './Thunk';
import { user } from 'types/QuanLyNguoiDung';
type QuanLyNguoiDung = {
  user? :user
}
const initialState :QuanLyNguoiDung = {
   user : JSON.parse(localStorage.getItem('USER'))
}

const quanLyNguoiDungReducer = createSlice({
  name: 'quanlynguoidung',
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder.addCase(LoginThunk.fulfilled,(state,{payload}) =>{
      state.user = payload 
      if (payload) {
        localStorage.setItem('USER',JSON.stringify(payload))
      }
    }).addCase(LoginThunk.rejected,(state ,{payload}) => {})

  }
});

export const {} = quanLyNguoiDungReducer.actions

export default quanLyNguoiDungReducer.reducer