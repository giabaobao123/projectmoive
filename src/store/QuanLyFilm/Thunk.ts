import { createAsyncThunk } from "@reduxjs/toolkit";
import { resolve } from "path";
import { QuanLyPhimService } from "services";
import { promise } from "zod";

export const getListFilmThunk = createAsyncThunk('QuanLyPhim/LayDanhSachPhim',async(_,{rejectWithValue}) => {
     try {
        const result = await QuanLyPhimService.getAllList();
        await new Promise((resolve) =>  setTimeout(resolve,1000))
        return result.data.content
     }
     catch(err) {
        return rejectWithValue(err)
     }
} )