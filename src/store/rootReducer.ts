import {combineReducers} from '@reduxjs/toolkit'
import quanLyNguoiDungReducer from './QuanLyNguoiDung/slice'
import quanLyPhimSlice from './QuanLyFilm/slice'

export const rootReducer = combineReducers({
    quanLyNguoiDungToolkit : quanLyNguoiDungReducer,
    quanLyPhimToolKit : quanLyPhimSlice
    
})