import { apiInstance } from "constant"
import { ListFilm } from "types/QuanLyPhim"
const api = apiInstance({
    baseURL : import.meta.env.VITE_QUAN_LY_PHIM_API
})
export const QuanLyPhimService = {
    getAllList : () => {
        return api.get<ApiReponse<ListFilm[]>>('/LayDanhSachPhim?maNhom=GP08')
    }
}