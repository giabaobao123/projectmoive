import { apiInstance } from "constant"
import { user } from "types/QuanLyNguoiDung"

const api = apiInstance({
    baseURL : 'https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung'
})
export const quanLyNguoiDungService = {
    register: (payload)=> api.post('/dangky',payload) ,
    login : (payload) => api.post<ApiReponse<user>>('dangnhap',payload),

}