import { HomTemPlate } from "components/templates";
import { PATH } from "constant";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

type Props = {};
const Home = (props: Props) => {
  const navigate = useNavigate();
  useEffect(() => {
    if (!JSON.parse(localStorage.getItem("USER"))) {
      navigate(PATH.login);
    }
  }, []);
  return (
    <div>
      <HomTemPlate />
    </div>
  );
};

export default Home;
