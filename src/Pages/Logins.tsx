import { useNavigate } from "react-router-dom";
import { PATH } from "../constant/config";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { LoginSchema } from "schema/LoginSchema";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useEffect } from "react";
import { useAppDispatch } from "store";
import { LoginThunk } from "store/QuanLyNguoiDung/Thunk";
import { Input } from "components/ui/Input";

export const Logins = () => {
  const navigate = useNavigate();
  useEffect(() => {
    if (localStorage.getItem("token")) {
      navigate("/");
    }
  }, []);
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm({
    mode: "onChange",
    resolver: zodResolver(LoginSchema),
  });
  const dispacth = useAppDispatch();
  const onSubmit = async (value) => {
    dispacth(LoginThunk(value))
      .unwrap()
      .then(() => {
        toast.success("Đăng nhập thành công");
        navigate("/");
      })
      .catch((err) => toast.error(err.response.data.content));

    // try {
    //   const result = await quanLyNguoiDungService.login(values);
    //   toast.success("Đăng nhập thành công");
    //   localStorage.setItem("token", `${result?.data?.content?.accessToken}`);
    //   setTimeout(() => {
    //     navigate("/home");
    //   }, 3000);
    // } catch (err) {
    //   toast.error(err.response.data.content);
    // }
  };
  return (
    <form className="pt-[30px] pb-[60px]" onSubmit={handleSubmit(onSubmit)}>
      <h1 className="text-white text-40 font-600">Sign In</h1>
      <div className="mt-20">
        {/* <div>
          <input
            placeholder="userName"
            type="text"
            className="outline-nopne block w-full p-15 border text-white border-white rounded-lg bg-[#333] focus:ring-blue-600"
            {...register("taiKhoan")}
          />
          <p className="text-red-600">{errors?.taiKhoan?.message as string}</p>
        </div> */}
        <Input
          register={register}
          error={errors?.taiKhoan?.message as string}
          name="taiKhoan"
          type="text"
          placeholder="Tài Khoản"
        />
        <Input
          register={register}
          error={errors?.matKhau?.message as string}
          placeholder="Mật khẩu"
          name="matKhau"
          type="password"
        />
        <div className="mt-40">
          <button className="text-white bg-red-500 font-500 rounded text-20 w-full p-10">
            Sign In
          </button>
        </div>
        <p className="text-white p-[10px] font-700">
          Chưa có tài khoản?
          <span
            className="text-blue-500 cursor-pointer"
            onClick={() => navigate(PATH.resgister)}
          >
            Đăng ký
          </span>
        </p>
      </div>
    </form>
  );
};
